<!-- ----------------------------------------------------------------------- -->

# *Wireless*HART Network Manager

<!-- ----------------------------------------------------------------------- -->

## Overview

This repository contains the software of a network manager compatible with
*Wireless*HART protocol, which can be customized to allow modifications in its
main tasks to carry out improvements within this protocol.

This network manager enables new possibilities for studies that explore new
management algorithms and proposes new techniques to improve the performance of
Industrial Wireless Networks. Aspects that before could only be analyzed
analytically or simulated, such as routing, scheduling, coexistence management,
energy, and mobility management, can now be tested in a real network
environment.

The network topology, according to the WirelessHART protocol, is shown in the
figure below.

<p align="center">
  <img width="40%" src="topology.png">
</p>

The application code is present in the repository, as well as the binaries for
running the Network Manager, Gateway, and Network Access Point Host
applications.

## Clone and Build

```
git clone https://gitlab.com/lascarwh/wh-netmgr.git
cd wh-netmgr/Linux/build
make
```

## Run

Below are the steps to run the applications and thus start the *Wireless*HART
network.

**Gateway**

```
cd wh-netmgr/Linux/gtw
./gtw
```

**Network Access Point**

```
cd wh-netmgr/Linux/gtw
sudo ./nap
```

**Network Manager**

```
cd wh-netmgr/Linux/gtw
./netmgr
```

**Application**

```
cd wh-netmgr/Linux/gtw
./app
```

## Acknowledgment

To cite this work, the BibTeX entry shown below can be used.

**Development of a Network Manager Compatible with *Wireless*HART Standard**

[![DOI](https://zenodo.org/badge/DOI/10.48011/asba.v2i1.1284.svg)](https://doi.org/10.48011/asba.v2i1.1284)

```
@inproceedings{Cainelli2020,
  doi = {10.48011/asba.v2i1.1284},
  url = {https://doi.org/10.48011/asba.v2i1.1284},
  year = {2020},
  month = dec,
  publisher = {sbabra},
  author = {Gustavo Cainelli and Max Feldman and Tiago Rodrigo Cruz and Ivan Muller and Carlos Eduardo Pereira},
  title = {Development of a Network Manager Compatible with {WirelessHART Standard},
  booktitle = {Anais do Congresso Brasileiro de Autom{\'{a}}tica 2020}
}
```

## Questions and Collaborations

If you have any questions or want to collaborate with this work, feel free to
contact the authors of the cited paper.
