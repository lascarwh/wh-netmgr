/* $COPYRIGHT */

#include <stdbool.h>
#include <stdint.h>
#include <netinet/in.h>
#include <sys/un.h>

#ifndef __CHAN_H
#define	__CHAN_H

#ifdef __cplusplus
extern		"C"
{
#endif

// Channel flags
/*!
 * @name Channel flags
 * \def CHAN_BIND
 *  Channel is bound
 * \def CHAN_CONN
 *  Channel is connected
 * \def CHAN_PEND
 *  Channel connection is pending
 */
#define	CHAN_BIND		0x00000001
#define	CHAN_CONN		0x00000002
#define	CHAN_PEND		0x00000004

typedef struct chan_address_ipv4_t
{
		struct sockaddr_in sa;
} chan_address_ipv4_t;

typedef struct chan_address_t
{
	chan_address_ipv4_t ipv4;
	char *filesystem_path; /* Must be set to NULL for network connections */
} chan_address_t;

typedef	int32_t	cid_t;

#define	CHAN_RD		0x01

typedef struct cid_list
{
	uint8_t	num_cids;
	cid_t	cids[0];
} cid_list_t;

/* chan.c */
bool		chan_init(void);
cid_t		chan_open(chan_address_t *);
int32_t		chan_close(cid_t);
int32_t		chan_listen(cid_t);
int32_t		chan_read(cid_t, void *, int);
int32_t		chan_write(cid_t, void *, int);
int32_t		chan_ready(cid_list_t *);
int32_t 	chan_get_desc(cid_t);
int 		chan_conn(cid_t);
cid_t		chan_open_sock(int32_t);
uint32_t	chan_stat(cid_t);
int			make_named_socket(const char *);

#define GW_NETMGR_DIRECT_SOCKET  "/tmp/gwsocket"
#define NETMGR_GW_DIRECT_SOCKET  "/tmp/netmgrsocket"

#ifdef __cplusplus
}
#endif
#endif /* __CHAN_H */
