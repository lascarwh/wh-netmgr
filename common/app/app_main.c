/* $COPYRIGHT */

/*!
 * \file app_main.c
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <termios.h>
#include <time.h>
#include <math.h>

#include <asm/param.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <fcntl.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sched.h>

#include "app.h"

#include "chan.h"


#include <ctype.h>

/*!
 * \struct qguid
 * \brief Discovery Query GUID
 *
 * \b Description
 * 		   This structure describes the content of the UDP multicast datagram
 * 		   used for Discovery Query.
 */
static const uint8_t qguid[16] =
{
	0x4c, 0x0f, 0x38, 0xac, 				// part one
	0x48, 0xee,								// part two
	0x49, 0x35,								// part three
	0xb6, 0x89,								// part four
	0x8f, 0x21,	0xf8, 0x5f, 0xc0, 0x30		// part five
};

/*!
 * \fn nap_gateway_discovery(uint16_t *appgw_port, uint32_t *appgw_addr, uint16_t napgw_nick)
 * \param appgw_port address of Gateway IP port
 * \param appgw_addr address of Gateway IP address
 * \param napgw_nick nickname of NAP
 * \brief Gateway Discovery Protocol
 *
 * \b Description
 * 		   This function implements the Gateway Discovery protocol as per
 * 		   Wireless Devices Specification (SPEC-290)
 *
 * \warning
 *
 * 		   The calling arguments are used to pass an IP port and address
 * 		   in to and out of this function. Initially, the IP port and address
 * 		   of the Gateway UDP multicast service are passed in. Once the
 * 		   service response has been received by the NAP, an IP address and
 * 		   port will be passed back and must be used to establish the connection
 * 		   for 'normal' Gateway - NAP communications traffic.
 */
static bool
// app_gateway_discovery(uint16_t *appgw_port, uint32_t *appgw_addr, uint16_t napgw_nick)
app_gateway_discovery(uint16_t *appgw_port, uint32_t *appgw_addr)
{
	int					sd;
	int					rc;
	struct sockaddr_in	sin;
	struct in_addr		in;
	uint8_t				ttl;

	sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
	if (sd < 0)
	{
		perror("socket multicast");

		return (0);
	}

	sin.sin_family = AF_INET;
	sin.sin_port = 0;
	sin.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(sd, (struct sockaddr *)&sin, sizeof (sin)) < 0)
	{
		perror("bind");
		close(sd);

		return (0);
	}

	// Use the default multicast interface
	in.s_addr = INADDR_ANY;

	rc = setsockopt(sd, IPPROTO_IP, IP_MULTICAST_IF, &in , sizeof (in));
	if (rc < 0)
	{
		perror("setsockopt IP_MULTICAST_IF");
		close(sd);
		return (0);
	}

	// Set the scope to 5
	ttl = 5;			// XXX: configuration?

	rc = setsockopt(sd, IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof (ttl));
	if (rc < 0)
	{
		perror("setsockopt IP_MULTICAST_TTL");
		close(sd);
		return (0);
	}

	// Setup the destination multicast address. The calling arguments
	// to this function will specify the IP address and port on which
	// the Gateway is servicing multicast traffic.
	sin.sin_family = AF_INET;
	sin.sin_port = *appgw_port;
	sin.sin_addr.s_addr = *appgw_addr;

	while (1)
	{
		struct timeval		timeout;
		uint8_t				buf[128];
		uint8_t				bufsz;
		uint32_t			nfds;
		fd_set				rfds;

		bufsz = sizeof (qguid);

		memcpy(&buf[0], &qguid[0], bufsz);
		// memcpy(&buf[bufsz], (uint8_t *)&napgw_nick, 2);

		// bufsz += 2;

		// Send off the multicast request to the Gateway
		rc = sendto(sd, &buf[0], bufsz, 0, (struct sockaddr *)&sin, sizeof(sin));
		if (rc < 0)
		{
			perror("sendto");
			close(sd);

			return (0);
		}

		timeout.tv_sec = 10;		// XXX: configuration?
		timeout.tv_usec = 0;		// XXX: configuration?

		FD_ZERO(&rfds);
		FD_SET(sd, &rfds);

		nfds = sd + 1;

		// Wait for a response from the Gateway or until the timer pops
		rc = select(nfds, &rfds, 0, 0, &timeout);
		if (rc < 0)
		{
			perror("select");
			close(sd);

			return (0);
		}

		// Check for data on the read side of the socket
		if (FD_ISSET(sd, &rfds))
		{
			// Grab the candidate response from the Gateway
			rc = recv(sd, &buf[0], sizeof (buf), 0);
			if (rc < 0)
			{
				perror("recvfrom");
				close(sd);

				return (0);
			}

			// Verify the length of the response and also verify that it
			// has the correct GUID from the Gateway.
			if (rc != (sizeof (qguid) + sizeof (uint16_t) + sizeof (uint32_t)) ||
			    (memcmp(&buf[0], &qguid[0], sizeof (qguid)) != 0))
			{
				fprintf(stderr, "nap_gateway_discovery: malformed response message\n");

				// The verification failed, so continue to loop until we
				// get a correct response.
				continue;
			}

			// We've got a verified response, so save the IP address and
			// port of where the Gateway wants us to connect to. Note that
			// the IP address and port are stored in network byte order.
			// Also be aware of where the IP address and port are stored,
			// i.e., via the calling argument pointers!
			memcpy(appgw_addr, &buf[16], 4);
			memcpy(appgw_port, &buf[20], 2);

			break;
		}
	}

	close(sd);

	return (1);
}

/*!
 * \fn nap_gateway(uint16_t appgw_port, uint32_t appgw_addr, uint16_t napgw_nick)
 * \param appgw_port Gateway IP port
 * \param appgw_address Gateway IP address
 * \brief Establish communications with Gateway
 *
 * \b Description
 * 	       This function establishes communications with the Gateway.
 *
 * \return
 *         If successful, a non-zero value is returned. This value
 *         is the channel identifier that was returned in the call
 *         to \c chan_open. If the function fails, a -1 is returned.
 */
static cid_t
// nap_gateway(uint16_t appgw_port, uint32_t appgw_addr, uint16_t napgw_nick)
app_gateway(uint16_t appgw_port, uint32_t appgw_addr)
{
	chan_address_t	address;
	cid_t			gtw_cid;
	uint32_t		flag;
	uint32_t		gtw_desc;

	// Get the IP port and address that the Gateway and NAP will
	// use to communicate over.
	if (!app_gateway_discovery(&appgw_port, &appgw_addr))
	{
		fprintf(stderr, "app_gateway: Gateway discovery failed\n");
		return (-1);
	}

	// Fill in the IP address and port of that was discovered
	memset(&address, 0, sizeof (chan_address_t));
	address.ipv4.sa.sin_port = appgw_port;
	address.ipv4.sa.sin_addr.s_addr = appgw_addr;

	// Open a communications channel
	gtw_cid = chan_open(&address);
	if (gtw_cid < 0)
	{
		fprintf(stderr, "app_gateway: chan_open failed\n");
		return (-1);
	}

	// Establish communications
	if (chan_conn(gtw_cid) < 0)
	{
		fprintf(stderr, "app_gateway: chan_conn failed\n");
		chan_close(gtw_cid);
		return (-1);
	}

	// Get the underlying descriptor, i.e., TCP socket
	gtw_desc = chan_get_desc(gtw_cid);

	// Disable the Nagle Algorithm
	flag = 1;			// XXX: configuration?

	if (setsockopt(gtw_desc, IPPROTO_TCP, TCP_NODELAY, &flag, sizeof (flag)) < 0)
	{
		perror("nap_gateway: setsockopt failed");
		chan_close(gtw_cid);
		return (-1);
	}

	return (gtw_cid);
}

// int				serial_fd;
// int				tcset;
// struct termios	otios;

/*!
 * \fn sighdlr(int signo)
 * \param signo signal the occured
 * \brief Signal handler
 *
 * \b Description
 *         This function handles one of the signals that will cause
 *         termination of the NAP. The purpose of this function is
 *         to restore the tty, i.e., serial, settings of the port that
 *         is used to communication with the NAP-RCP.
 */
// static void
// sighdlr(int signo)
// {
// 	signo = signo;
//
// 	// restore the orginal settings
// 	if (tcsetattr(serial_fd, TCSANOW, &otios))
// 	{
// 		perror("sighdlr: tcsetattr failed");
// 	}
// }

/*!
 * \fn nap_rcp(char *naprcp_dev)
 * \param naprcp_dev NAP-RCP device
 * \brief Establish communications with the NAP-RCP
 *
 * \b Description
 * 		   This function establishes communications with the Gateway.
 *
 * \return
 * 		   If successful, a non-zero value is returned. This value
 *         is the channel identifier that was returned in the call
 *         to \c chan_open. If the function fails, a -1 is returned.
 */
// static cid_t
// nap_rcp(char *naprcp_dev)
// {
// 	chan_address_t		address;
// 	cid_t				rcp_cid;
// 	struct sigaction	sa;
// 	struct termios		ntios;
//
// 	address.filesystem_path = naprcp_dev;
//
// 	rcp_cid = chan_open(&address);
// 	if (rcp_cid < 0)
// 	{
// 		fprintf(stderr, "nap_rcp: chan_open failed\n");
// 		return (-1);
// 	}
//
// 	serial_fd = chan_get_desc(rcp_cid);
//
// 	tcset = 0;
//
// 	sa.sa_handler = sighdlr;
// 	sigemptyset(&sa.sa_mask);
// 	sa.sa_flags = 0;
//
// 	if (sigaction(SIGINT, &sa, (struct sigaction *)0))
// 	{
// 		perror("nap_rcp: sigaction failed SIGINT\n");
// 		goto restore;
// 	}
//
// 	if (sigaction(SIGTERM, &sa, (struct sigaction *)0))
// 	{
// 		perror("nap_rcp: sigaction failed SIGTERM\n");
// 		goto restore;
// 	}
//
// 	if (sigaction(SIGQUIT, &sa, (struct sigaction *)0))
// 	{
// 		perror("nap_rcp: sigaction failed SIGQUIT\n");
// 		goto restore;
// 	}
//
// 	if (tcgetattr(serial_fd, &otios))
// 	{
// 		perror("nap_rcp: tcgetattr failed");
// 		goto errout;
// 	}
//
// 	memcpy((void *)&ntios, &otios, sizeof (struct termios));
//
// 	if (cfsetispeed(&ntios, B115200))
// 	{
// 		perror("cfsetispeed failed");
// 		goto restore;
// 	}
//
// 	ntios.c_iflag |=  (IGNBRK | IGNPAR);
// 	ntios.c_iflag &= ~(ICRNL | IXON);
//
// 	ntios.c_oflag &= ~OPOST;
//
// 	ntios.c_lflag &= ~(ISIG | ICANON | ECHO | IEXTEN);
//
// 	ntios.c_cc[VTIME] = 2;
// 	//ntios.c_cc[VMIN] = 10;
//
// 	// Controle de fluxo por hardware....
// 	//ntios.c_cflag |= (CRTSCTS);
//
// 	if (tcsetattr(serial_fd, TCSANOW, &ntios))
// 	{
// 		perror("tcsetattr failed");
// 		goto restore;
// 	}
//
// 	tcset = 1;
//
// 	return (rcp_cid);
//
// restore:
// 	if (tcsetattr(serial_fd, TCSANOW, &otios))
// 	{
// 		perror("tcsetattr failed");
// 	}
//
//
// errout:
// 	return (-1);
// }

// static	uint8_t		bufq[256];
// static	uint16_t	bufqlen;
// static	bool		bufact;

/*!
 * \fn nap_proc_gtw(cid_t gtw_cid, cid_t rcp_cid)
 * \param gtw_cid channel identifier for Gateway
 * \param rcp_cid channel identifier for NAP-RCP
 * \brief Gateway -> NAP-Host / NAP-RCP message processor.
 *
 * \b Description
 *		   This function is the message processor for messages
 *		   arriving from the Gateway for the NAP-Host / NAP-RCP.
 *
 * \return
 * 		   If successful, a zero is returned. Otherwise, -1 is
 * 		   returned to indicate an error has occured.
 */
static int32_t
// app_proc_gtw(cid_t gtw_cid)
app_proc_gtw(cid_t gtw_cid, int * cmd_rc, int * cmd_resp)
{
	int			rc;
	uint8_t		mtyp;
	uint8_t	   *p;
	uint8_t		ibuf[128], obuf[256];
	uint16_t	ocnt;

	// Read the message from the Gateway
	rc = chan_read(gtw_cid, (void *)&ibuf[0], sizeof (ibuf));
	if (rc < 0)
	{
		fprintf(stderr, "app_proc_gtw: chan_read failed\n");
		return (-1);
	}
	else if (rc == 0)
	{
		fprintf(stderr, "app_proc_gtw: GATEWAY DISAPPEARED\n");
		return (-1);
	}

	// TODO
	// printf("\nTRAFFIC from GATEWAY (%s)\n", __FUNCTION__);
	// printf("--------------------------------------------------");
	// for (size_t k = 0; k < rc; k++) {
	// 	if (k % 10 == 0)
	// 		printf("\n");
	// 	printf("[%02X] ", ibuf[k]);
	// }
	// printf("\n--------------------------------------------------\n");

	*cmd_rc = ibuf[3];

	// printf("\n# RESPONSE\n\n");

	// printf("RC = %d\n", ibuf[3]);
	// printf("DATA = ");

	*cmd_resp = rc - 4; // response data size - byte 0

	for (size_t k = 0; k < rc - 4; k++) {
		*(cmd_resp + k + 1) = ibuf[k + 4];
	}

	// printf("\n\n\nPressione ENTER para continuar...");
	// getchar();

	return (0);
}


int send_cmd(cid_t gtw_cid, cid_list_t * cidl, int cmd, int nick, char * data_buf, int * cmd_resp)
{

	int rc, size=0;
	bool cmd_send = false;

	uint32_t	ltmp;
	uint16_t	stmp;
	uint8_t buf[128];

	int cmd_rc;

	// ---------------------------------------------------------------------------

	// Destination = Field Device
	buf[size] = 2;
	size += 1;

	// Size
	size += 2;

	// Transport Type
	buf[size] = 0x02; //NET_TRANSPORT_REQ_UNICAST
	size += 1;

	// Transport Handle
	buf[size] = 0x77;
	size += 1;

	// Priority
	ltmp = htonl(3);
	memcpy(&buf[size], &ltmp, 4);
	size += 4;

	// Address Type - Nickname
	ltmp = htonl(2);
	memcpy(&buf[size], &ltmp, 4);
	size += 4;

	// Nickname
	stmp = htons(nick);
	memcpy(&buf[size], &stmp, 2);
	size += 2;

	// No proxy
	size += 2;

	// Command
	stmp = htons(cmd);
	memcpy(&buf[size], &stmp, 2);
	size += 2;

	// Command Data Size + Data

	memcpy(&buf[size], data_buf, data_buf[0] + 1);

	size += data_buf[0] + 1;

	// Size
	stmp = htons(size);
	memcpy(&buf[1], &stmp, 2);

	// *********************************************************************

	rc = chan_write(gtw_cid, &buf, size);

	if (rc < 0)
	{
		fprintf(stderr, "%s: chan_write failed\n", __FUNCTION__);
		assert(0);
	}

	cmd_send = true;

	while (cmd_send) {
		int32_t		rc;
		int32_t		nchans;

		cidl->num_cids = 1;
		cidl->cids[0] = gtw_cid;

		// Wait for a message
		nchans = chan_ready(cidl);
		if (nchans < 0)
		{
			fprintf(stderr, "chan_ready failed\n");
			break;
		}

		// Message from Gateway
		if (cidl->cids[0] != -1)
		{
			rc = app_proc_gtw(gtw_cid, &cmd_rc, cmd_resp);
			cmd_send = false;
			if (rc < 0)
			{
				fprintf(stderr, "app_proc: fatal error with Gateway\n");
				break;
			}
		}
	}

	return cmd_rc;
}


int cmd_data(char * buffer)
{
	char str_input[256];
	int buffer_size = 0;

	// Clear line buffer
	int c;
	while ( (c = getchar()) != EOF && c != '\n') { }

	fgets(str_input, sizeof str_input, stdin);

	// remove "/n" no final da string
	str_input[strlen(str_input) - 1] = '\0';

	// data bytes
	buffer_size = ( strlen(str_input) / 2 ) + ( strlen(str_input) % 2 );

	// adiciona 0 no início no caso impar
	if ( (strlen(str_input) % 2) != 0 )
	{
		char temp[256];
		strcpy(temp, str_input);
		memset(str_input, 0, sizeof str_input);
		str_input[0] = '0';
		strcat(str_input, temp);
	}

	buffer[0] = buffer_size;

	char temp[3];

	for(int i=0; i<strlen(str_input)/2; i++)
	{
		temp[0] = str_input[i * 2];
		temp[1] = str_input[(i * 2) + 1];
		temp[2] = '\0';
		buffer[i+1] = strtol(temp, NULL, 16);
	}

	return buffer_size;
}


int device_list(cid_t gtw_cid, cid_list_t * cidl, uint16_t * dev_list2)
{

	char data_buf[128];
	memset(data_buf, 0, sizeof data_buf);

	int cmd_rc;
	int cmd_resp[128];

	// uint8_t dev_list[20]; // 10 devices

	// NAP
	uint8_t dev_count = 1;

	// nap nick
	// dev_list[0] = 0x12;
	// dev_list[1] = 0x34;

	// Get Devices
	int cmd = 780;
	int nick = 0x1234;
	// data para o 780
	data_buf[0] = 2; 	// 2 bytes
	data_buf[1] = 0x00; // index
	data_buf[2] = 0xFF; // número de leituras

	cmd_rc = send_cmd(gtw_cid, cidl, cmd, nick, data_buf, cmd_resp);

	// adiciona número de vizinhos do NAP ao dev_count
	dev_count += cmd_resp[2]; // nap(1) + vizinhos(cmd_resp[2])

	// NAP
	dev_list2[0] = nick;

	// criar lista de devices
	for (size_t k = 0; k < dev_count-1; k++) {
		// dev_list[k*2+2] = cmd_resp[4+10*k];
		// dev_list[k*2+3] = cmd_resp[5+10*k];
		dev_list2[k+1] = (cmd_resp[4+10*k]<<8) + cmd_resp[5+10*k];
	}

	// printf("Device List (%02d)\n", dev_count);
	// for (size_t k = 0; k < dev_count; k++) {
	// 	printf("%02d. [0x%02X%02X]\n", k+1, dev_list[k*2], dev_list[k*2+1]);
	// }
	// printf("\n");
	//
	// printf("Device List2 (%02d)\n", dev_count);
	// for (size_t k = 0; k < dev_count; k++) {
	// 	printf("%02d. [0x%04X]\n", k+1, dev_list2[k]);
	// }
	// printf("\n");

	return(dev_count);

}

/*
 * Read ED Function
 */
void read_ed(cid_t gtw_cid, cid_list_t * cidl, uint16_t dev, uint8_t * ed_list)
{
	// printf("--> %s\n", __FUNCTION__);
	printf("* 0x%04X\n", dev);

	char data_buf[128];
	memset(data_buf, 0, sizeof data_buf);

	int cmd_resp[128];
	memset(cmd_resp, 0, sizeof cmd_resp);

	int cmd = 127;
	int cmd_rc;

	// Read ED cmd
	// Se falhar - reenvia o comando
	while (cmd_resp[0] != 3*CH_MAP_SIZE) {
		cmd_rc = send_cmd(gtw_cid, cidl, cmd, dev, data_buf, cmd_resp);
		printf("* RC = %d (%d)\n", cmd_rc, cmd_resp[0]);
	}

	// TODO!!!
	// cmd_resp -> ed_list
	for (size_t i = 0; i < cmd_resp[0]/3; i++)
	{
		ed_list[cmd_resp[i*3 + 1] - 11] = cmd_resp[i*3 + 3];
	}

	// testes - ed falso!
	// for (size_t i = 0; i < 15; i++) {
	// 	ed_list[i] = dev - 0x1234;
	// }

	// DEBUG
	for (size_t i = 0; i < 15; i++) {
		printf("[0x%02X]", ed_list[i]);
	}
	printf("\n");

	// printf("<-- %s\n", __FUNCTION__);
}

/*
 * Read ED to All Network Devices
 */
void read_ed_all(cid_t gtw_cid, cid_list_t * cidl, uint16_t * dev_list, uint8_t * ed_list, int dev_count)
{
	// printf("--> %s\n", __FUNCTION__);

	// ED - 2D array
	// uint8_t ed_temp[dev_count][15] = {0}; // Original
	uint8_t ed_temp[dev_count-1][15] = {0}; // NAP removido
	uint16_t ed_total[15] = {0};

	// read ed (all devices)
	// for (size_t i = 0; i < dev_count; i++) { // Original
	for (size_t i = 0; i < dev_count-1; i++) { // NAP removido
	// 	read_ed(gtw_cid, cidl, dev_list[i], ed_temp[i]); // Original
		read_ed(gtw_cid, cidl, dev_list[i+1], ed_temp[i]); // NAP removido
	}


	// Avarage ED (all devices) -> ed_list
	for (size_t i = 0; i < 15; i++) {
		// for (size_t j = 0; j < dev_count; j++) { // Original
		for (size_t j = 0; j < dev_count-1; j++) { // NAP removido
			ed_total[i] += ed_temp[j][i];
			// ed_list[i] += ed_temp[j][i];
		}
		// ed_list[i] = ed_list[i] / dev_count; // Original
		ed_list[i] = ed_total[i] / (dev_count-1); // NAP removido
	}

	// printf("<-- %s\n", __FUNCTION__);

}

void channel_ed_sort(uint8_t ed[][15], float * ed_means, int * ch_id)
{

	uint16_t ed_sum[CH_MAP_SIZE];
	memset(ed_sum, 0, sizeof(ed_sum));


	// soma dos canais
	for (size_t i = 0; i < MEASUREMENT_CYCLES; i++) {
		for (size_t j = 0; j < CH_MAP_SIZE; j++) {
			ed_sum[j] += ed[i][j];
		}
	}

	// calculo das medias
	for (size_t i = 0; i < CH_MAP_SIZE; i++) {
		ed_means[i] = (ed_sum[i]/MEASUREMENT_CYCLES);
	}

	// sort
	for(int i=0; i<CH_MAP_SIZE; i++)
	{
		for(int j=0; j<CH_MAP_SIZE-1; j++)
		{
			if(ed_means[j]<ed_means[j+1])
			{
				float temp=ed_means[j];
				int temp_id=ch_id[j];
				ed_means[j]=ed_means[j+1];
				ch_id[j]=ch_id[j+1];
				ed_means[j+1]=temp;
				ch_id[j+1]=temp_id;
			}
		}
	}

}

// *****************************************************************************
// Channel Map Select Functions - START
// *****************************************************************************

uint16_t ch_map_select_01(uint8_t ed[][15])
{

	uint16_t channel_map = 0x7FFF;

	float ed_means[CH_MAP_SIZE];

	int ch_id[CH_MAP_SIZE];
	for (int i = 0; i < CH_MAP_SIZE; i++) {
		ch_id[i] = i;
	}

	channel_ed_sort(ed, ed_means, ch_id);

	float ed_mean = 0;
	float ed_min = ed_means[0];

	for (size_t i = 0; i < CH_MAP_SIZE; i++) {
		ed_mean += ed_means[i];
	}

	// ed mean -> sum / channels
	ed_mean /= CH_MAP_SIZE;

	for (size_t i = 0; i < CH_MAP_SIZE; i++) {

		if ((ed_means[i] > (ed_mean*1.5)) | (ed_means[i] > (ed_min*3))) {

			channel_map &= ~(1 << ch_id[i]);

		}

	}

	return channel_map;

}

// *****************************************************************************

uint16_t ch_map_select_02(uint8_t ed[][15])
{

	uint16_t channel_map = 0x7FFF;
	uint16_t ed_mean = 0;

	float ed_means[CH_MAP_SIZE];

	int ch_id[CH_MAP_SIZE];
	for (int i = 0; i < CH_MAP_SIZE; i++) {
		ch_id[i] = i;
	}

	channel_ed_sort(ed, ed_means, ch_id);

	for (size_t i = 0; i < MAX_BLACKLIST_SIZE; i++) {

		for (size_t j = i+1; j < CH_MAP_SIZE; j++) {
			ed_mean += ed_means[j];
		}

		ed_mean /= (CH_MAP_SIZE - (i+1));

		if (ed_means[i] > (1.5*ed_mean)) {
			channel_map &= ~(1 << ch_id[i]);
		}

		ed_mean = 0;

	}

	return channel_map;

}

// *****************************************************************************

uint16_t ch_map_select_anova(uint8_t ed[][15])
{

	uint16_t channel_map = 0x7FFF;
	float tc;
	uint16_t soma_total = 0;
	uint32_t soma_quad = 0;
	float sq_ch = 0;
	float sq_e = 0;
	float mq_ch, mq_e, f_calc;
	bool ch_flag = false;

	uint16_t soma_ch[CH_MAP_SIZE];
	memset(soma_ch, 0, sizeof(soma_ch));

	// sem interferencia
	// ed[0][0]  = 0x27; ed[1][0]  = 0x30; ed[2][0]  = 0x34;
	// ed[0][1]  = 0x2A; ed[1][1]  = 0x0B; ed[2][1]  = 0x2C;
	// ed[0][2]  = 0x2C; ed[1][2]  = 0x2A; ed[2][2]  = 0x07;
	// ed[0][3]  = 0x2E; ed[1][3]  = 0x27; ed[2][3]  = 0x29;
	// ed[0][4]  = 0x2A; ed[1][4]  = 0x3A; ed[2][4]  = 0x21;
	// ed[0][5]  = 0x2D; ed[1][5]  = 0x39; ed[2][5]  = 0x2A;
	// ed[0][6]  = 0x25; ed[1][6]  = 0x2D; ed[2][6]  = 0x3E;
	// ed[0][7]  = 0x2C; ed[1][7]  = 0x34; ed[2][7]  = 0x36;
	// ed[0][8]  = 0x31; ed[1][8]  = 0x2D; ed[2][8]  = 0x0B;
	// ed[0][9]  = 0x31; ed[1][9]  = 0x2C; ed[2][9]  = 0x34;
	// ed[0][10] = 0x27; ed[1][10] = 0x2B; ed[2][10] = 0x30;
	// ed[0][11] = 0x35; ed[1][11] = 0x35; ed[2][11] = 0x2C;
	// ed[0][12] = 0x30; ed[1][12] = 0x04; ed[2][12] = 0x36;
	// ed[0][13] = 0x02; ed[1][13] = 0x33; ed[2][13] = 0x33;
	// ed[0][14] = 0x3F; ed[1][14] = 0x3B; ed[2][14] = 0x2D;

	// com interferencia
	// ed[0][0]  = 0x24; ed[1][0]  = 0x1E; ed[2][0]  = 0x1B;
	// ed[0][1]  = 0x21; ed[1][1]  = 0x21; ed[2][1]  = 0x21;
	// ed[0][2]  = 0x21; ed[1][2]  = 0x21; ed[2][2]  = 0x21;
	// ed[0][3]  = 0x18; ed[1][3]  = 0x27; ed[2][3]  = 0x1E;
	// ed[0][4]  = 0x2D; ed[1][4]  = 0x21; ed[2][4]  = 0x21;
	// ed[0][5]  = 0x2A; ed[1][5]  = 0x1B; ed[2][5]  = 0x24;
	// ed[0][6]  = 0x27; ed[1][6]  = 0x1E; ed[2][6]  = 0x15;
	// ed[0][7]  = 0x24; ed[1][7]  = 0x21; ed[2][7]  = 0x12;
	// ed[0][8]  = 0x63; ed[1][8]  = 0x66; ed[2][8]  = 0x66;
	// ed[0][9]  = 0x6F; ed[1][9]  = 0x6C; ed[2][9]  = 0x69;
	// ed[0][10] = 0x0F; ed[1][10] = 0x1E; ed[2][10] = 0x24;
	// ed[0][11] = 0x1E; ed[1][11] = 0x21; ed[2][11] = 0x1E;
	// ed[0][12] = 0x18; ed[1][12] = 0x33; ed[2][12] = 0x0F;
	// ed[0][13] = 0x27; ed[1][13] = 0x15; ed[2][13] = 0x1B;
	// ed[0][14] = 0x24; ed[1][14] = 0x18; ed[2][14] = 0x27;

	// DEBUG
	// printf("//////// Matriz de Experimentos ////////\n");
	// for (size_t i = 0; i < 3; i++) {
	// 	for (size_t j = 0; j < 15; j++) {
	// 		printf("[%02X]", ed[i][j]);
	// 	}
	// 	printf("\n");
	// }
	// printf("\n");

	for (size_t i = 0; i < MEASUREMENT_CYCLES; i++) {
		for (size_t j = 0; j < CH_MAP_SIZE; j++) {

			soma_total += ed[i][j];
			soma_quad += (ed[i][j]*ed[i][j]);
			soma_ch[j] += ed[i][j];

		}
	}

	// termo de correcao
	tc = (soma_total*soma_total)/(MEASUREMENT_CYCLES*CH_MAP_SIZE);
	// printf("tc = %f\n", tc);

	// sq_ch
	for (size_t i = 0; i < CH_MAP_SIZE; i++) {
		sq_ch += (float)(soma_ch[i]*soma_ch[i]);
	}

	sq_ch = (sq_ch/MEASUREMENT_CYCLES) - tc;
	// printf("sq_ch = %f\n", sq_ch);

	// sq_erro
	sq_e = soma_quad - sq_ch - tc;
	// printf("sq_e = %f\n", sq_e);

	// mq_ch
	mq_ch = sq_ch / (CH_MAP_SIZE - 1);
	// printf("mq_ch = %f\n", mq_ch);

	// mq_e
	mq_e = sq_e / ((CH_MAP_SIZE) * (MEASUREMENT_CYCLES - 1));
	// printf("mq_e = %f\n", mq_e);

	// f_calc
	f_calc = mq_ch / mq_e;
	printf("f_calc = %f\n", f_calc);

	if (f_calc > F_TAB) {
		ch_flag = true;
	}

	if (ch_flag) {
		printf("\033[0;32m"); // green
		printf("significativo!!\n");
		printf("\033[0m");
	} else {
		printf("\033[0;31m"); // red
		printf("nao significativo!!\n");
		printf("\033[0m");
		return channel_map;
	}

	// ***************************************************************************

	// Teste de Tukey
	// TSD - Tukey Significant Difference

	float tsd = Q_STUDENTIZED * sqrt(mq_e/MEASUREMENT_CYCLES);
	printf("tsd = %f\n", tsd);

	float ed_means[CH_MAP_SIZE];

	int ch_id[CH_MAP_SIZE];
	for (int i = 0; i < CH_MAP_SIZE; i++) {
		ch_id[i] = i;
	}

	channel_ed_sort(ed, ed_means, ch_id);

	// DEBUG
	for (size_t i = 0; i < CH_MAP_SIZE; i++) {
		printf("[%f]", ed_means[i]);
	}
	printf("\n");
	for (size_t i = 0; i < CH_MAP_SIZE; i++) {
		printf("[%d]", ch_id[i]);
	}
	printf("\n");

	// ***************************************************************************

	// // metodo 1 - remover do maior para o menor
	//
	// // MAX_BLACKLIST_SIZE = 10
	// for (size_t i = 0; i < MAX_BLACKLIST_SIZE; i++) {
	//
	// 	// channel -> blacklist
	// 	channel_map &= ~(1 << ch_id[i]);
	//
	// 	// se encontrar media diferente - parar o teste
	// 	if ( (abs(ed_means[i]-ed_means[i+1])) > tsd) {
	// 		// printf("parou com i=%d\n", i);
	// 		break;
	// 	}
	// }
	//
	// printf("teste 1 = 0x%04X\n", channel_map);
	//
	// // metodo 2 - testar a partir do menor e remover todos quando mudar a media
	//
	// channel_map = 0x7FFF;
	bool blacklist_flag = false;

	for (size_t i = (CH_MAP_SIZE-1)-1; i != -1; i--) {

		if(!blacklist_flag) {
			blacklist_flag = ( abs(ed_means[i]-ed_means[i+1]) > tsd );
		}

		// printf("i = %d | blacklist_flag = %d\n", i, blacklist_flag);

		// MAX_BLACKLIST_SIZE = 10
		if( blacklist_flag && (i < MAX_BLACKLIST_SIZE) ) {
			channel_map &= ~(1 << ch_id[i]);
		}

	}

	// printf("teste 2 = 0x%04X\n", channel_map);

	return channel_map;

}

// *****************************************************************************

// uint16_t ch_map_select_fdp(uint8_t * ed_list, int * ch_id)
uint16_t ch_map_select_fdp(uint8_t ed[][15])
{

	uint16_t channel_map = 0x7FFF;

	uint32_t ed_list_sum = 0;

	float fdp_lim = 0.6;

	float ed_list_norm[CH_MAP_SIZE];
	float ed_means[CH_MAP_SIZE];

	int ch_id[CH_MAP_SIZE];
	for (int i = 0; i < CH_MAP_SIZE; i++) {
		ch_id[i] = i;
	}

	channel_ed_sort(ed, ed_means, ch_id);

	// DEBUG
	// for (size_t i = 0; i < CH_MAP_SIZE; i++) {
	// 	printf("[%f]", ed_means[i]);
	// }
	// printf("\n");
	// for (size_t i = 0; i < CH_MAP_SIZE; i++) {
	// 	printf("[%d]", ch_id[i]);
	// }
	// printf("\n");

	// inicializacao da lista normalizada
	for (size_t i = 0; i < CH_MAP_SIZE; i++)
		ed_list_norm[i] = ed_means[i];

	// encontrar o minimo
	// for (size_t i = 0; i < CH_MAP_SIZE; i++)
	// 	ed_list_norm[i] = ed_means[i];

	uint8_t ed_min = 0xFF;

	for (size_t i = 0; i < CH_MAP_SIZE; i++) {
		if (ed_means[i] < ed_min) {
			ed_min = ed_means[i];
		}
	}

	// subtrair minimo
	for (size_t i = 0; i < CH_MAP_SIZE; i++)
		ed_list_norm[i] -= ed_min;

	// calculo da soma
	for (size_t i = 0; i < CH_MAP_SIZE; i++)
		ed_list_sum += ed_list_norm[i];

	// DEBUG
	// for (size_t i = 0; i < CH_MAP_SIZE; i++) {
	// 	printf("[%.2f]", ed_list_norm[i]);
	// }
	// printf("\n");

	for (size_t i = 0; i < CH_MAP_SIZE; i++) {
		ed_list_norm[i] = ed_list_norm[i]/ed_list_sum;
	}

	// DEBUG
	// for (size_t i = 0; i < CH_MAP_SIZE; i++) {
	// 	printf("[%.2f]", ed_list_norm[i]);
	// }
	// printf("\n");

	float ed_norm_sum = 0;

	for (size_t i = 0; i < CH_MAP_SIZE; i++) {

		channel_map &= ~(1 << ch_id[i]);

		ed_norm_sum += ed_list_norm[i];

		// printf("**** ed_norm_sum = %f\n", ed_norm_sum);

		// FDP
		if (ed_norm_sum > fdp_lim) {
			break;
		}

	}

	return channel_map;

}

// *****************************************************************************

uint16_t ch_map_select_kworst(uint8_t ed[][15])
{

	uint16_t channel_map = 0x7FFF;

	int k = 4;

	float ed_means[CH_MAP_SIZE];

	int ch_id[CH_MAP_SIZE];

	for (int i = 0; i < CH_MAP_SIZE; i++) {
		ch_id[i] = i;
	}

	channel_ed_sort(ed, ed_means, ch_id);

	// for (size_t i = 0; i < CH_MAP_SIZE; i++) {
	// 	printf("[%d]", ch_id[i]);
	// }
	// printf("\n");

	for (size_t i = 0; i < k; i++) {
		channel_map &= ~(1 << ch_id[i]);
	}

	return channel_map;

}

// *****************************************************************************
// Channel Map Select Functions - END
// *****************************************************************************

/*
 * Create Channel Map
 */
uint16_t channel_map_create(uint8_t ed[][15])
{

	uint16_t channel_map;

	// Create Channel Map

	// FIXED CHANNEL MAP
	// channel_map = 0x7FF0; // testes

	// K-Worst
	// channel_map = ch_map_select_kworst(ed);
	// printf("---> channel_map_kworst = 0x%04X\n", channel_map);

	// FDP
	// channel_map = ch_map_select_fdp(ed);
	// printf("---> channel_map_fdp = 0x%04X\n", channel_map);

	// TESTE 1
	// channel_map = ch_map_select_01(ed);
	// printf("---> channel_map_01 = 0x%04X\n", channel_map);

	// TESTE 2
	// channel_map = ch_map_select_02(ed);
	// printf("---> channel_map_02 = 0x%04X\n", channel_map);

	// ANOVA
	channel_map = ch_map_select_anova(ed);
	printf("---> channel_map_anova = 0x%04X\n", channel_map);

	return channel_map;
}

/*
 * Update Channel Map
 */
void channel_map_update(cid_t gtw_cid, cid_list_t * cidl, uint16_t * dev_list, uint16_t channel_map, int dev_count)
{
	printf("--> %s\n", __FUNCTION__);

	char data_buf[128];
	memset(data_buf, 0, sizeof data_buf);

	int cmd_resp[128];
	memset(cmd_resp, 0, sizeof cmd_resp);

	int cmd, cmd_rc, nick;
	uint64_t asn0, asn1;

	// TODO: Adicionar cálculo do delay conforme número de FDs
	// delay = 10s
	// int slot_delay = 10 * 100; // seconds to slots
	// delay = 4s
	int slot_delay = 4 * 100; // seconds to slots

	char cmd150_data[8]; // size (1) + channel map (2) + asn (5)
	memset(cmd150_data, 0, sizeof(cmd150_data));
	cmd150_data[0] = 7;

	// channel map
	cmd150_data[1] = (channel_map >> 8) & 0xFF;
	cmd150_data[2] = (channel_map) & 0xFF;

	// Read ASN (NAP)
	cmd = 64598;
	nick = 0x1234;
	data_buf[0] = 0;

	cmd_rc = send_cmd(gtw_cid, cidl, cmd, nick, data_buf, cmd_resp);

	asn0 = cmd_resp[1];
	for(int i=2; i<=cmd_resp[0]; i++) {
		asn0 <<= 8;
		asn0 |= (uint64_t) cmd_resp[i];
	}
	asn1 = asn0 + slot_delay;

	// printf("asn inicial = %02X%08X\n", (int32_t)(asn0>>32), (uint32_t)(asn0&0x00000000FFFFFFFF));
	// printf("asn final = %02X%08X\n", (int32_t)(asn1>>32), (uint32_t)(asn1&0x00000000FFFFFFFF));

	cmd150_data[3] = (asn1 >> 32) & 0xFF;
	cmd150_data[4] = (asn1 >> 24) & 0xFF;
	cmd150_data[5] = (asn1 >> 16) & 0xFF;
	cmd150_data[6] = (asn1 >> 8) & 0xFF;
	cmd150_data[7] = (asn1) & 0xFF;

	// TODO - cmd150 to all devices
	cmd = 150;
	for (size_t i = 0; i < dev_count; i++) {
		nick = dev_list[i];
		send_cmd(gtw_cid, cidl, cmd, nick, cmd150_data, cmd_resp);
	}

	printf("<-- %s\n", __FUNCTION__);
}

// -----------------------------------------------------------------------------

/*
 * Read Transmits Counters
 */
void read_tx_counters(cid_t gtw_cid, cid_list_t * cidl, uint16_t * dev_list,
											int dev_count, uint16_t * tx_counter,
											uint16_t * tx_fails_counter)
{

	char data_buf[128];
	memset(data_buf, 0, sizeof data_buf);

	int cmd_resp[128];
	memset(cmd_resp, 0, sizeof cmd_resp);

	// Reset counters
	*tx_counter = 0;
	*tx_fails_counter = 0;

	int nick, cmd_rc;

	int cmd = 780;
	data_buf[0] = 2;
	data_buf[1] = 0x00;
	data_buf[2] = 0x01; // NAP

	// read failed transmits (all devices)
	for (size_t i = 0; i < dev_count-1; i++) {
		do {
			cmd_rc = send_cmd(gtw_cid, cidl, cmd, dev_list[i+1], data_buf, cmd_resp);
			printf("CMD_RC = %d (%d)\n", cmd_rc, cmd_resp[0]);
		} while (cmd_resp[0] != 13); // 13 - tamanho correto resposta 780
		*tx_counter += (cmd_resp[8] << 8) + cmd_resp[9];
		*tx_fails_counter += (cmd_resp[10] << 8) + cmd_resp[11];
	}

}

/*
 * Read Failed Transmits
 */
int read_failed_transmits(cid_t gtw_cid, cid_list_t * cidl, uint16_t * dev_list, int dev_count)
{

	char data_buf[128];
	memset(data_buf, 0, sizeof data_buf);

	int cmd_resp[128];
	memset(cmd_resp, 0, sizeof cmd_resp);

	int nick, cmd_rc;

	int cmd = 780;
	data_buf[0] = 2;
	data_buf[1] = 0x00;
	data_buf[2] = 0x01; // NAP

	uint16_t failed_transmits = 0;

	// read failed transmits (all devices)
	for (size_t i = 0; i < dev_count-1; i++) {
		cmd_rc = send_cmd(gtw_cid, cidl, cmd, dev_list[i+1], data_buf, cmd_resp);
		// printf("---> %d\n", cmd_rc);
		// printf("0x%02X%02X\n", cmd_resp[10], cmd_resp[11]);
		failed_transmits += (cmd_resp[10] << 8) + cmd_resp[11];
	}

	return failed_transmits;

}

/*
 * Read Failed Transmits - NAP
 */
int read_failed_transmits_nap(cid_t gtw_cid, cid_list_t * cidl, int dev_count)
{

	char data_buf[128];
	memset(data_buf, 0, sizeof data_buf);

	int cmd_resp[128];
	memset(cmd_resp, 0, sizeof cmd_resp);

	int nick, cmd_rc;

	int cmd = 780;
	data_buf[0] = 2;
	data_buf[1] = 0x00;
	data_buf[2] = dev_count-1;

	uint16_t failed_transmits = 0;

	// read failed transmits (NAP -> all devices)
	cmd_rc = send_cmd(gtw_cid, cidl, cmd, 0x1234, data_buf, cmd_resp);

	for (size_t i = 0; i < cmd_resp[3]; i++) {
		failed_transmits += (cmd_resp[10+(10*i)] << 8) + cmd_resp[11+(10*i)];
		// printf("0x%02X%02X\n", cmd_resp[10+(10*i)], cmd_resp[11+(10*i)]);
	}

	return failed_transmits;

}

/*
 * Read Transmits
 */
int read_transmits(cid_t gtw_cid, cid_list_t * cidl, uint16_t * dev_list, int dev_count)
{

	char data_buf[128];
	memset(data_buf, 0, sizeof data_buf);

	int cmd_resp[128];
	memset(cmd_resp, 0, sizeof cmd_resp);

	int nick, cmd_rc;

	int cmd = 780;
	data_buf[0] = 2;
	data_buf[1] = 0x00;
	data_buf[2] = 0x01; // NAP

	uint16_t transmits = 0;

	// read transmits (all devices)
	for (size_t i = 0; i < dev_count-1; i++) {
		cmd_rc = send_cmd(gtw_cid, cidl, cmd, dev_list[i+1], data_buf, cmd_resp);
		// printf("---> %d\n", cmd_rc);
		transmits += (cmd_resp[8] << 8) + cmd_resp[9];
	}

	return transmits;

}

/*
 * Read Transmits - NAP
 */
int read_transmits_nap(cid_t gtw_cid, cid_list_t * cidl, int dev_count)
{

	char data_buf[128];
	memset(data_buf, 0, sizeof data_buf);

	int cmd_resp[128];
	memset(cmd_resp, 0, sizeof cmd_resp);

	int nick, cmd_rc;

	int cmd = 780;
	data_buf[0] = 2;
	data_buf[1] = 0x00;
	data_buf[2] = dev_count-1;

	uint16_t transmits = 0;

	// read transmits (NAP -> all devices)
	cmd_rc = send_cmd(gtw_cid, cidl, cmd, 0x1234, data_buf, cmd_resp);

	for (size_t i = 0; i < cmd_resp[3]; i++) {
		transmits += (cmd_resp[8+(10*i)] << 8) + cmd_resp[9+(10*i)];
		// printf("0x%02X%02X\n", cmd_resp[8+(10*i)], cmd_resp[9+(10*i)]);
	}

	return transmits;

}

// *****************************************************************************

/*
 *	APPs
 */

/*
 * Command App
 * Device + CMD + Data
 */
static void cmd_app(cid_t gtw_cid, cid_list_t * cidl)
{
	//cid_list_t     *cidl;

	uint8_t buf[128];
	char data_buf[128];
	memset(data_buf, 0, sizeof data_buf);

	int cmd, nick = 0;
	uint8_t data;
	int rc, size;
	bool cmd_send = false;

	uint32_t	ltmp;
	uint16_t	stmp;

	int cmd_rc;
	int cmd_resp[128];
	memset(cmd_resp, 0, sizeof cmd_resp);

	memset(buf, 0, sizeof buf);

	while (1)
	{

		system("clear");
		printf("-------------------------------\n");
		printf("-- Wireless Hart Command App --\n");
		printf("-------------------------------\n\n");

		// Input

		// Device nickname
		printf("NICKNAME [HEX]: ");
		scanf("%x", &nick);

		// Command
		printf("CMD: ");
		scanf("%d", &cmd);

		// *********************************************************************

		// Data
		printf("DATA [HEX]: ");
		rc = cmd_data(data_buf);

		printf("\n# REQUEST\n\n");

		printf("DATA = ");

		for (size_t k = 0; k < rc; k++) {
			if (k % 10 == 0 && k !=0)
				printf("\n       ");
			printf("[%02X] ", data_buf[k+1]);
		}
		printf("\n");

		cmd_rc = send_cmd(gtw_cid, cidl, cmd, nick, data_buf, cmd_resp);

		printf("\n# RESPONSE\n\n");

		printf("RC = %d\n", cmd_rc);
		printf("DATA = ");

		for (size_t k = 0; k < cmd_resp[0]; k++) {
			if (k % 10 == 0 && k !=0)
				printf("\n       ");
			printf("[%02X] ", cmd_resp[k+1]);
		}
		printf("\n");

		printf("\n\n\nPressione ENTER para continuar...");
		getchar();
	}
}

// *****************************************************************************

/*!
 * \fn nap_procp(cid_t gtw_cid, cid_t rcp_cid)
 * \param gtw_cid channel identifier for Gateway
 * \param rcp_cid channel identifier for NAP-RCP
 * \brief NAP message processor
 *
 * \b Description
 *		   This function is the message processor for messages
 *		   arriving from either the Gateway or the NAP-RCP
 */
static void
app_proc(cid_t gtw_cid)
{
	cid_list_t     *cidl;
	cidl = (cid_list_t *)malloc(sizeof (cid_list_t) + (sizeof (cid_t)));
	int app_index;

	system("clear");
	printf("--------------\n");
	printf("-- App Menu --\n");
	printf("--------------\n\n");

	printf("1. Command App\n");

	printf("\nSelect App Index: ");
	scanf("%d", &app_index);

	switch (app_index) {
		case 1:
			printf("\nCommand App Selected!\n");
			sleep(1);
			cmd_app(gtw_cid, cidl);
			break;
		default:
			printf("\nInvalid Selection!!\n");
			sleep(1);
			break;
	}

	chan_close(gtw_cid);
	free(cidl);

}

/*!
 * \fn main(int argc, char **argv)
 * \brief Main line routine for Network Access Point (NAP) - Host
 *
 * \b Description
 * 		   Main line routine for the host implementation of the
 * 		   Network Access Point (NAP).
 */
int
main(void)
//main(int argc, char **argv)
{

	printf("APP MAIN!\n");

	cid_t			gtw_cid;
	uint16_t		appgw_port;
	uint32_t		appgw_addr;

	appgw_port = 0;
	appgw_addr = 0;

	appgw_addr = inet_addr("127.0.0.1");
	appgw_port = htons(55555);

	// Initialize the channel communication subsystem
	if (!chan_init())
	{
		fprintf(stderr, "channel subsystem failed to initialize\n");
		exit(1);
	}

	// Establish connection with the Gateway
	gtw_cid = app_gateway(appgw_port, appgw_addr);
	if (gtw_cid < 0)
	{
		fprintf(stderr, "app_gateway failed\n");
		exit(1);
	}

	// Service normal communications traffic
	app_proc(gtw_cid);

	exit(0);
}
