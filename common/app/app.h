/* $COPYRIGHT */
/*!
 * \file app.h
 * \brief App header file
 */

// -----------------------------------------------------------------------------

#ifndef __APP_H
#define __APP_H

// -----------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif

// -----------------------------------------------------------------------------

// CHANNEL MAP

#define	CH_MAP_SIZE           15
#define	MAX_BLACKLIST_SIZE    10
#define MEASUREMENT_DELAY     30

// Statistics

#define MEASUREMENT_CYCLES    3
// #define MEASUREMENT_CYCLES    1

// F_TAB(CH_MAP_SIZE-1, ((MEASUREMENT_CYCLES*CH_MAP_SIZE)-1) - (CH_MAP_SIZE-1))
// F_TAB(14,30) = 1.737
#define F_TAB                 1.737f

// Q_alpha(k, N-k) = q_0.01(15, 45-30) = 4.770
#define Q_STUDENTIZED         4.770f

// -----------------------------------------------------------------------------

// Complete Test

#define TEST_CYCLES			10

// -----------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif
#endif /* __APP_H */

// -----------------------------------------------------------------------------
